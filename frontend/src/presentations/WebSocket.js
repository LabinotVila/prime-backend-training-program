import React from "react";
import faker from "faker";
import withWebSocket from "middleware/withWebSocket";
import Typography from "presentations/Typography";
import { Highlighted } from "presentations/Label";
import { Button, TextField } from "@material-ui/core";
import classNames from "classnames";
import LoadingIndicator from "presentations/LoadingIndicator";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = ({ size, palette, typography }) => ({
  messages: {
    height: 320,
    background: 'white',
    width: '100%',
    overflow: 'auto',
    position: 'relative'
  },
  row: {
    marginBottom: 8,
    fontSize: size.defaultFontSize,
    color: palette.textColor
  },
  actions: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
    marginBottom: 8,
    width: 480,
    justifyContent: 'space-between',
    '& :first-child': {
      marginRight: 4,
      flex: 1
    },
    '& :last-child': {
      marginRight: 4,
    },
  },
  send: {
    color: palette.leadColor,
    fontWeight: 'bold'
  },
  received: {
    color: palette.success,
  }
})

const WebSocket = (props) => {
  const { classes, endpoint, readOnly = false } = props
  const [text, setText] = React.useState('Simple Socket Message from ' + faker.name.findName())
  const [messages, setMessages] = React.useState([])
  const list = React.useRef()

  const onMessage = (message) => {
    setMessages((state) => ([...state, { text: message }]))
  }

  const { socket, onRetry, open, loading} = withWebSocket(endpoint, onMessage)
  const onSendClicked = (event) => {
    socket.send(text)
    setMessages((state) => ([...state, { text, send: true }]))
  }

  const onClearClicked = (event) => {
    setMessages([])
  }

  const onKeyPress = (event) => {
    if (event.key !== 'Enter') {
      return
    }
    event.preventDefault();
    onSendClicked(event)
  }

  const onChange = (event) => {
    setText(event.target.value)
  }

  React.useEffect(() => {
    if (!!list.current) {
      list.current.scrollTop = list.current.scrollHeight
    }
  }, [messages])


  const ready = loading || open
  return (
    <React.Fragment>
      {!readOnly && <div className={classes.actions}>
        <TextField margin="none" name="name" onKeyPress={onKeyPress} onChange={onChange} value={text} label="Message"/>
        <Button disabled={!ready} onClick={onSendClicked}>Send</Button>
      </div>}
      <div ref={list} className={classes.messages}>
        {messages.map((next, index) => <div className={classNames(classes.row, next.send ? classes.send : classes.received)} key={index}>{next.send ? 'Send' : 'Received'} #{index + 1}: {next.text}</div>)}
        <LoadingIndicator isInside={true} show={loading}/>
      </div>
      <Typography variant="caption">
        This is a websocket connection at <Highlighted>ws://localhost:9000/api{endpoint}</Highlighted>:
      </Typography>
      {ready ? <Button onClick={onClearClicked}>Clear</Button> : <Button onClick={onRetry}>Retry</Button>}
    </React.Fragment>
  )
}

export default withStyles(styles)(WebSocket)
