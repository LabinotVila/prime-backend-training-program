/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "../../presentations/rows/SimpleLink";
import { Bold, Highlighted } from "../../presentations/Label";
import Code from "../../presentations/Code";

const styles = ({ size, typography }) => ({
  root: {
  },
  img: {
    boxShadow: 'none'
  }
})

const mongoCodec = `CodecProvider pojoCodecProvider =
  PojoCodecProvider.builder()
    .conventions(Collections.singletonList(ANNOTATION_CONVENTION))
    .register("io.training.models")
    .automatic(true)
    .build();
final CodecRegistry customEnumCodecs = CodecRegistries.fromCodecs();
CodecRegistry pojoCodecRegistry = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), customEnumCodecs, CodecRegistries.fromProviders(pojoCodecProvider));`

const simpleExample = `@Data
@RequiredArgsConstructor
public class Person {
	@NonNull
	private String firstName;
	@NonNull
	private String lastName;
	private Address address = null;
}`

const enumExample = `public enum Membership {
	UNREGISTERED,
	SUBSCRIBER,
	PREMIUM
}

@Data
@RequiredArgsConstructor
public class Person {
	@NonNull
	private String firstName;
	@NonNull
	private String lastName;
	@NonNull
	private Membership membership;
	private Address address = null;
}`

const usersClassess = `@Data
@ToString
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, property="type", visible = true)
@JsonSubTypes({
  @JsonSubTypes.Type(value= FreeUser.class, name = "FREE"),
  @JsonSubTypes.Type(value= PremiumUser.class, name = "PREMIUM"),
  @JsonSubTypes.Type(value= SubscriberUser.class, name = "SUBSCRIBER")
})
@BsonDiscriminator(key = "type", value = "NONE")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegisteredUser {
	@BsonId
	@JsonSerialize(using = ObjectIdStringSerializer.class)
	@JsonDeserialize(using = ObjectIdDeSerializer.class)
	public ObjectId id;
	String name;
	@BsonIgnore
	Membership type = Membership.NONE;
}

@BsonDiscriminator(key = "type", value = "FREE")
@Data
public class FreeUser extends RegisteredUser {
	@Override
	public Membership getType() { return Membership.FREE; }
}

@BsonDiscriminator(key = "type", value = "PREMIUM")
@Data
public class PremiumUser extends RegisteredUser {
	private String card;
	@Override
	public Membership getType() { return Membership.PREMIUM; }
}

@BsonDiscriminator(key = "type", value = "SUBSCRIBER")
@Data
public class SubscriberUser extends RegisteredUser {
	@Override
	public Membership getType() { return Membership.SUBSCRIBER; }
}`

const registering = `ClassModel<RegisteredUser> registeredUserModel = ClassModel.builder(RegisteredUser.class).enableDiscriminator(true).build();
ClassModel<FreeUser> freeUserModel = ClassModel.builder(FreeUser.class).enableDiscriminator(true).build();
ClassModel<SubscriberUser> subscriberUserModel = ClassModel.builder(SubscriberUser.class).enableDiscriminator(true).build();
ClassModel<PremiumUser> premiumUserModel = ClassModel.builder(PremiumUser.class).enableDiscriminator(true).build();

CodecProvider pojoCodecProvider =
    PojoCodecProvider.builder()
        .conventions(Collections.singletonList(ANNOTATION_CONVENTION))
        .register("io.training.models")
        .register(registeredUserModel, freeUserModel, subscriberUserModel, premiumUserModel)
        .automatic(true)
        .build();`


const pojoExercise = `public class TaxiPojoMongoController extends Controller {

	public Result all (Http.Request request) {
		return ok(Json.toJson(new ArrayList<>()));
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result update (Http.Request request, String id) {
		return ok(Json.newObject());
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result delete (Http.Request request, String id) {
		return ok(Json.newObject());
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result save (Http.Request request) {
		return ok(Json.newObject());
	}

}`

const SimpleQueries = (props) => {
  const { classes, section } = props
  const pojos = section.children[0]
  const filters = section.children[1]
  const sorting = section.children[2]
  const crud = section.children[3]
  return (
    <Fragment>
      <Typography variant={'heading'}>
        Section 9: {section.display}
        <Divider />
      </Typography>

      <Typography id={pojos.id} variant={'title'}>
        {pojos.display}
        <Typography>
          <SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/bson/pojos/">https://mongodb.github.io/mongo-java-driver/4.1/bson/pojos/</SimpleLink>
        </Typography>
      </Typography>
      <Typography>
        The 3.5 release of the driver adds POJO support via the PojoCodecProvider, which allows for direct serialization of POJOs and Java Beans to and from BSON. Internally, the Codec for each POJO utilizes a ClassModel instance to store metadata about how the POJO should be serialized.
      </Typography>
      <Typography>
        A <Highlighted>ClassModel</Highlighted> for a POJO includes:
        <ul>
          <li>The class of the POJO.</li>
          <li>A new instance factory. Handling the creation of new instances of the POJO. By default it requires the POJO to have an empty constructor.</li>
          <li>Property information, a list of PropertyModel instances that contain all the property metadata. By default this includes; any public getter methods with any corresponding setter methods and any public fields.</li>
          <li>An optional IdProperty (see BsonId annotation). By default the _id or id property in the POJO.</li>
          <li>Type data for the POJO and its fields to work around type erasure.</li>
          <li>An optional discriminator value. The discriminator is the value used to represent the POJO class being stored.</li>
          <li>An optional discriminator key. The document key name for the discriminator.</li>
          <li>The use discriminator flag. This determines if the discriminator should be serialized. By default it is off.</li>
          <li>An optional IdGenerator used to generate the id value, when inserting the POJO. (New in 3.10)</li>
        </ul>
        Each <Highlighted>PropertyModel</Highlighted> includes:
        <ul>
          <li>The property name.</li>
          <li>The read name, the name of the property to use as the key when serializing into BSON.</li>
          <li>The write name, the name of the property to use as the key when deserializing from BSON.</li>
          <li>Type data, to work around type erasure.</li>
          <li>An optional Codec for the property. The codec allows for fine grained control over how the property is encoded and decoded.</li>
          <li>A serialization checker. This checks if the value should be serialized. By default, null values are not serialized.</li>
          <li>A property accessor. Used to access the property values from the POJO instance.</li>
          <li>Use discriminator flag, only used when serializing other POJOs. By default it is off. When on the PojoCodecProvider copies the ClassModel for the field’s type and turns on the use discriminator flag. The corresponding ClassModel must be configured with adiscriminator key and value.</li>
        </ul>
        ClassModels are built using the <Highlighted>ClassModelBuilder</Highlighted> which can be accessed via the <Highlighted>ClassModel.builder(clazz)</Highlighted> method. The builder initially uses reflection to create the required metadata.
      </Typography>
      <Typography variant="section">
        POJO support
      </Typography>
      <Typography>
        Automatic POJO support can be provided by setting <Highlighted>PojoCodecProvider.Builder#automatic(true)</Highlighted>, once built the <Highlighted>PojoCodecProvider</Highlighted> will automatically create a POJO <Highlighted>Codec</Highlighted> for any class that contains at least one serializable or deserializable property.
      </Typography>
      <Typography>
        At our backend server we are using the following configuration:
        <Code>
          {mongoCodec}
        </Code>
      </Typography>
      <Typography variant="section">
        Default configuration
      </Typography>
      <Typography>
        Let us take the following example:
        <Code>
          {simpleExample}
        </Code>
        The instance of <Highlighted>new Person("Ada", "Lovelace");</Highlighted> would be serialized to the equivalent of <Highlighted>{`{ firstName: "Ada", lastName: "Lovelace"}`}</Highlighted>.
      </Typography>
      <Typography>
        Notice the <Highlighted>address</Highlighted> property is omitted because it hasn’t been set and has a <Highlighted>null</Highlighted> value. If the person instance contained an address, it would be stored as a sub document and use the <Highlighted>CodecRegistry</Highlighted> to look up the <Highlighted>Codec</Highlighted> for the <Highlighted>Address</Highlighted> class and use that to encode and decode the address value.
      </Typography>

      <Typography variant="section">
        POJO Properties
      </Typography>
      <Typography>
        Properties are identified by public <Bold>getter</Bold> methods, public <Bold>setter</Bold> methods and <Bold>public</Bold> fields.<br/>
        Any properties with an underlying field that is transient or static will be ignored and not serialized or deserialized.
        <ol>
          <li><Bold>Serializing to BSON</Bold> If a public getter methods exists, it is used to obtain the value for the property, otherwise the field is used directly.</li>
          <li><Bold>Deserializing from BSON</Bold> If a public setter method exists it is used to set the value, otherwise the field is set directly.</li>
        </ol>
      </Typography>
      <Typography variant="section">
        Enum support
      </Typography>
      <Typography>
        Enums are fully supported. The POJO <Highlighted>Codec</Highlighted> uses the name of the enum constant as the property value. This is then converted back into an Enum value by the codec using the static <Highlighted>Enum.valueOf</Highlighted> method.
      </Typography>
      <Typography>
        Example:
        <Code>
          {enumExample}
        </Code>
        The instance of <Highlighted>new Person("Bryan", "May", SUBSCRIBER);</Highlighted> would be serialized to the equivalent of <Highlighted>{`{ firstName: "Bryan", lastName: "May", membership: "SUBSCRIBER"}`}</Highlighted>.
      </Typography>
      <Typography>
        If you require an alternative representation of the Enum, you can override how a Enum is stored by registering a custom <Highlighted>Codec</Highlighted> for the Enum in the <Highlighted>CodecRegistry</Highlighted>.
      </Typography>
      <Typography variant="section">
        Conventions
      </Typography>
      <Typography>
        The <Highlighted><SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/bson/org/bson/codecs/pojo/Convention.html">Convention</SimpleLink></Highlighted> interface provides a mechanism for <Highlighted>ClassModelBuilder</Highlighted> instances to be configured during the build stage and the creation of the <Highlighted>ClassModel</Highlighted>.
      </Typography>
      <Typography>
        The following Conventions are available from the Conventions class:
        <ol>
          <li>The <Highlighted><SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/bson/org/bson/codecs/pojo/Conventions.html#ANNOTATION_CONVENTION">ANNOTATION_CONVENTION</SimpleLink></Highlighted>. Applies all the default annotations.</li>
          <li>The <Highlighted><SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/bson/org/bson/codecs/pojo/Conventions.html#CLASS_AND_PROPERTY_CONVENTION">CLASS_AND_PROPERTY_CONVENTION</SimpleLink></Highlighted>. Sets the discriminator key if not set to _t and the discriminator value if not set to the ClassModels simple type name. Also, configures the PropertyModels. If the idProperty isn’t set and there is a property named _id or id then it will be marked as the idProperty.</li>
          <li>The <Highlighted><SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/bson/org/bson/codecs/pojo/Conventions.html#SET_PRIVATE_FIELDS_CONVENTION">SET_PRIVATE_FIELDS_CONVENTION</SimpleLink></Highlighted>. Enables private fields to be set directly using reflection, without the need of a setter method. Note this convention is not enabled by default.</li>
          <li>The <Highlighted><SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/bson/org/bson/codecs/pojo/Conventions.html#USE_GETTERS_FOR_SETTERS">USE_GETTERS_FOR_SETTERS</SimpleLink></Highlighted> convention. Allows getters to be used for Map and Collection properties without setters, the collection/map is then mutated. Note this convention is not enabled by default.</li>
          <li>The <Highlighted><SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/bson/org/bson/codecs/pojo/Conventions.html#OBJECT_ID_GENERATORS">OBJECT_ID_GENERATORS</SimpleLink></Highlighted> convention. Adds an IdGenerator that generates new ObjectId for ClassModels that have an ObjectId value for the id property.</li>
          <li>The <Highlighted><SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/bson/org/bson/codecs/pojo/Conventions.html#DEFAULT_CONVENTIONS">DEFAULT_CONVENTIONS</SimpleLink></Highlighted>, a list containing the <Highlighted>ANNOTATION_CONVENTION</Highlighted>, the <Highlighted>CLASS_AND_PROPERTY_CONVENTION</Highlighted> and the <Highlighted>OBJECT_ID_GENERATORS</Highlighted> convention.</li>
          <li>The <Highlighted><SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/bson/org/bson/codecs/pojo/Conventions.html#NO_CONVENTIONS">NO_CONVENTIONS</SimpleLink></Highlighted> an empty list.</li>
        </ol>
      </Typography>
      <Typography variant="section">
        Advanced configuration
      </Typography>
      <Typography>
        For most scenarios there is no need for further configuration. However, there are some scenarios where custom configuration is required.
      </Typography>
      <Typography>
        As an example lets say that we are working with 3 different types of Users. A Free User, a Subscriber and a Premium User. In order to work with different users within the same collection a custom discriminator can be used to identify which user is which, like this:
        <Code>
          {usersClassess}
        </Code>
        And then register them like this:
        <Code>
          {registering}
        </Code>
        Lets look at the <Highlighted>/api/lecture9/pojo/user</Highlighted> routes (GET and POST) and investigate these behaviours!
      </Typography>
      <Typography id={filters.id} variant={'title'}>
        {filters.display}
        <Typography>
          <ul>
            <li><SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/builders/filters/">https://mongodb.github.io/mongo-java-driver/4.1/builders/filters/</SimpleLink></li>
            <li><SimpleLink href="https://docs.mongodb.com/manual/tutorial/query-documents/">https://docs.mongodb.com/manual/tutorial/query-documents/</SimpleLink></li>
          </ul>
        </Typography>
      </Typography>
      <Typography>
        The <SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/mongodb-driver-core/com/mongodb/client/model/Filters.html">Filters</SimpleLink> class provides static factory methods for all the MongoDB query operators. Each method returns an instance of the Bson type, which can in turn be passed to any method that expects a query filter.
      </Typography>
      <Typography>
        For brevity, you may choose to import the methods of the Filters class statically:
      </Typography>
      <Code>
        import static com.mongodb.client.model.Filters.*;
      </Code>
      <Typography>
        All the examples below assume this static import.
      </Typography>
      <Typography>
        <Bold>Comparison</Bold>
        <ul>
          <li><Bold>eq</Bold>: Matches values that are equal to a specified value.</li>
          <li><Bold>gt</Bold>: Matches values that are greater than a specified value.</li>
          <li><Bold>gte</Bold>: Matches values that are greater than or equal to a specified value.</li>
          <li><Bold>lt</Bold>: Matches values that are less than a specified value.</li>
          <li><Bold>lte</Bold>: Matches values that are less than or equal to a specified value.</li>
          <li><Bold>ne</Bold>: Matches all values that are not equal to a specified value.</li>
          <li><Bold>in</Bold>: Matches any of the values specified in an array.</li>
          <li><Bold>nin</Bold>: Matches none of the values specified in an array.</li>
        </ul>
        <Bold>Logical</Bold>
        <ul>
          <li><Bold>and</Bold>: Joins filters with a logical AND and selects all documents that match the conditions of both filters.</li>
          <li><Bold>or</Bold>: Joins filters with a logical OR and selects all documents that match the conditions of either filters.</li>
          <li><Bold>not</Bold>: Inverts the effect of a query expression and selects documents that do not match the filter.</li>
          <li><Bold>nor</Bold>: Joins filters with a logical NOR and selects all documents that fail to match both filters.</li>
        </ul>
        <Bold>Arrays</Bold>
        <ul>
          <li><Bold>all</Bold>: Matches arrays that contain all elements specified in the query</li>
          <li><Bold>elemMatch</Bold>: Selects documents if element in the array field matches all the specified $elemMatch conditions</li>
          <li><Bold>size</Bold>: Selects documents if the array field is a specified size</li>
        </ul>
        <Bold>Elements</Bold>
        <ul>
          <li><Bold>exists</Bold>: Selects documents that have the specified field.</li>
          <li><Bold>type</Bold>: Selects documents if a field is of the specified type.</li>
        </ul>
        <Bold>Evaluation</Bold>
        <ul>
          <li><Bold>mod</Bold>: Performs a modulo operation on the value of a field and selects documents with a specified result.</li>
          <li><Bold>regex</Bold>: Selects documents where values match a specified regular expression.</li>
          <li><Bold>text</Bold>: Selects documemts matching a full-text search expression.</li>
          <li><Bold>where</Bold>: Matches documents that satisfy a JavaScript expression.</li>
        </ul>
        <Bold>Bitwise</Bold>
        <ul>
          <li><Bold>bitsAllSet</Bold>: Selects documents where the all the specified bits of a field are set (i.e. 1).</li>
          <li><Bold>bitsAllClear</Bold>: Selects documents where the all the specified bits of a field are clear (i.e. 0).</li>
          <li><Bold>bitsAnySet</Bold>: Selects documents where at least one of the specified bits of a field are set (i.e. 1).</li>
          <li><Bold>bitsAnyClear</Bold>: Selects documents where at least one of the specified bits of a field are clear (i.e. 0)</li>
        </ul>
        <Bold>Geospatial</Bold>
        <ul>
          <li><Bold>geoWithin</Bold>: Selects all documents containing a field whose value is a GeoJSON geometry that falls within within a bounding GeoJSON geometry.</li>
          <li><Bold>geoWithinBox</Bold>: Selects all documents containing a field with grid coordinates data that exist entirely within the specified box.</li>
          <li><Bold>geoWithinPolygon</Bold>: Selects all documents containing a field with grid coordinates data that exist entirely within the specified polygon.</li>
          <li><Bold>geoWithinCenter</Bold>: Selects all documents containing a field with grid coordinates data that exist entirely within the specified circle.</li>
          <li><Bold>geoWithinCenterSphere</Bold>: Selects geometries containing a field with geospatial data (GeoJSON or legacy coordinate pairs) that exist entirely within the specified circle, using spherical geometry.</li>
          <li><Bold>geoIntersects</Bold>: Selects geometries that intersect with a GeoJSON geometry. The 2dsphere index supports $geoIntersects.</li>
          <li><Bold>near</Bold>: Returns geospatial objects in proximity to a point. Requires a geospatial index. The 2dsphere and 2d indexes support $near.</li>
          <li><Bold>nearSphere</Bold>: Returns geospatial objects in proximity to a point on a sphere. Requires a geospatial index. The 2dsphere and 2d indexes support $nearSphere.</li>
        </ul>
      </Typography>
      <Typography id={sorting.id} variant={'title'}>
        {sorting.display}
        <Typography>
          <SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/builders/sorts/">https://mongodb.github.io/mongo-java-driver/4.1/builders/sorts/</SimpleLink>
        </Typography>
      </Typography>
      <Typography>
        The <SimpleLink href="https://mongodb.github.io/mongo-java-driver/4.1/apidocs/mongodb-driver-core/com/mongodb/client/model/Sorts.html">Sorts</SimpleLink> class provides static factory methods for all the MongoDB sort criteria operators. Each method returns an instance of the Bson type, which can in turn be passed to any method that expects sort criteria.
      </Typography>
      <Typography>
        For brevity, you may choose to import the methods of the <Highlighted>Sorts</Highlighted> class statically:
      </Typography>
      <Typography>
        All the examples below assume this static import.
      </Typography>
      <Typography variant="section">
        Ascending
      </Typography>
      <Typography>
        To specify an ascending sort, use one of the ascending methods.
      </Typography>
      <Typography>
        This example specifies an ascending sort on the quantity field:
        <Code>
          ascending("quantity")
        </Code>
        This example specifies an ascending sort on the quantity field, followed by an ascending sort on the totalAmount field:
        <Code>
          ascending("quantity", "totalAmount")
        </Code>
      </Typography>
      <Typography variant="section">
        Descending
      </Typography>
      <Typography>
        To specify a descending sort, use one of the descending methods.
      </Typography>
      <Typography>
        This example specifies a descending sort on the quantity field:
        <Code>
          descending("quantity")
        </Code>
        This example specifies a descending sort on the quantity field, followed by a descending sort on the totalAmount field:
        <Code>
          descending("quantity", "totalAmount")
        </Code>
      </Typography>
      <Typography variant="section">
        Text Score
      </Typography>
      <Typography>
        To specify a sort by the <SimpleLink href="https://docs.mongodb.com/manual/reference/operator/query/text/#sort-by-text-search-score">score of a $text query</SimpleLink>, use the metaTextScore method to specify the name of the projected field.
      </Typography>
      <Typography>
        This example specifies a sort on the score of a $text query that will be projected into the scoreValue field in a projection on the same query:
        <Code>
          metaTextScore("scoreValue")
        </Code>
      </Typography>
      <Typography variant="section">
        Combining sort criteria
      </Typography>
      <Typography>
        To specify the combination of multiple sort criteria, use the orderBy method.
      </Typography>
      <Typography>
        This example specifies an ascending sort on the quantity field, followed by an ascending sort on the totalAmount field, followed by a descending sort on the orderDate field:
        <Code>
          orderBy(ascending("quantity", "totalAmount"), descending("orderDate"))
        </Code>
      </Typography>
      <Typography id={crud.id} variant={'title'}>
        {crud.display}
      </Typography>
      <Typography>
        Let us do an exercise on the lecture:
        <Code>
          {pojoExercise}
        </Code>
      </Typography>
    </Fragment>
  )
}

export default withStyles(styles)(SimpleQueries)
