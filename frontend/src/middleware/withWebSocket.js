import React from "react";
import { API_URL } from 'Constants'


const domain = API_URL.replace('http://', 'ws://').replace('https://', 'wss://')
const PING = "PING"
const PONG = "PONG"

/**
 * A simple websocket connector
 * @param url
 * @param onMessage
 * @returns {{webSocket, onRetry: connect, open: boolean, loading: boolean}}
 */
const withWebSocket = (url, onMessage) => {
  const socketUrl = `${domain}${url}`

  const [state, setState] = React.useState(false)
  const [loading, setLoading] = React.useState(false)
  const ping = React.useRef()
  const webSocket = React.useRef()

  const close = (which) => {
    if (!which) {
      return
    }
    which.onclose = () => {}
    which.onopen = () => {}
    which.onmessage = () => {}
    which.close()
  }

  const dispose = () => {
    if (!!ping.current) {
      clearInterval(ping.current)
      ping.current = null
    }
    if (!!webSocket.current) {
      close(webSocket.current)
      webSocket.current = null
    }
    setState(false)
  }

  const connect = () => {
    dispose()

    const socket = new WebSocket(socketUrl)
    console.log('opening websocket', socketUrl)
    webSocket.current = socket
    setLoading(true)

    socket.onopen = () => {
      console.log('on socket open')
      ping.current = setInterval(() => {
        console.log('Sending ping!')
        socket.send(PING)
      }, 5000)
      setState(true)
      setLoading(false)
    }
    socket.onmessage = (event) => {
      if (event.data === PONG) {
        console.log('Received pong!')
        return
      }
      console.log('on socket message', event.data)
      if (!onMessage) {
        return
      }
      onMessage(event.data)
    }
    socket.onclose = () => {
      console.log('on socket closed')
      setLoading(false)
      dispose()
    }
  }

  React.useEffect(() => {
    connect()
    return () => {
      dispose()
    }
  }, [url])

  return { socket: webSocket.current, onRetry: connect, open: state, close, loading }
}

export default withWebSocket
