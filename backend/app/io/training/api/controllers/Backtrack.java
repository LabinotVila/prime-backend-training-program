package io.training.api.controllers;

import io.training.api.models.backtracking.Node;
import io.training.api.models.backtracking.Nodes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Backtrack {
    private static List<Node> visited = new ArrayList<>();
    private static List<List<Integer>> solution = new ArrayList<>();

    public static boolean backtrack(List<Node> nodes, Node currentNode,  Node lastNode)
    {
        if (currentNode.getId() == lastNode.getId()) {

            List<Integer> list = visited.stream().map(Node::getId).collect(Collectors.toList());
            solution.add(list);

            return false;
        }

        for (int i = 0; i < currentNode.getLinks().size(); i++) {

            int thisNodeIndex = currentNode.getLinks().get(i);
            Node thisNode = nodes.stream().filter(x -> x.getId() == thisNodeIndex).findFirst().get();

            if (!visited.contains(thisNode)) {
                visited.add(thisNode);

                if (backtrack(nodes, thisNode, lastNode)) return true;

                visited.remove(thisNode);
            }
        }

        return false;
    }

    public static List<List<Integer>> solve(Nodes problem) {

        Optional<Node> startNode = problem.getNodes().stream().filter(node -> node.getId() == problem.getFrom()).findFirst();
        if (!startNode.isPresent()) return new ArrayList<>();

        Optional<Node> lastNode = problem.getNodes().stream().filter(node -> node.getId() == problem.getTo()).findFirst();
        if (!lastNode.isPresent()) return new ArrayList<>();

        visited.add(startNode.get());

        backtrack(problem.getNodes(), startNode.get(), lastNode.get());

        return solution;
    }

}
