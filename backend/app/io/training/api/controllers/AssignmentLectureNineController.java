package io.training.api.controllers;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import io.training.api.models.User;
import io.training.api.models.validators.HibernateValidator;
import io.training.api.mongo.MongoDB;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.types.ObjectId;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static play.mvc.Results.*;

public class AssignmentLectureNineController {
    @Inject
    MongoDB mongoDB;

    @Inject
    HttpExecutionContext ec;

    public CompletionStage<Result> setup(Http.Request request) {
        return CompletableFuture
                .supplyAsync(() -> mongoDB.getMongoDatabase())
                .thenApply(training -> {
                    User[] users = Json.fromJson(request.body().asJson(), User[].class);

                    MongoCollection<User> dbUsers = training.getCollection("users", User.class);

                    for (User user : users) dbUsers.insertOne(user);

                    return Json.toJson(users);
                })
                .thenApply(Results::ok)
                .exceptionally(result -> badRequest(result.getLocalizedMessage()));
    }

    public CompletionStage<Result> all(Http.Request request) {
        return CompletableFuture
                .supplyAsync(() -> mongoDB.getMongoDatabase())
                .thenApply(training -> training.getCollection("users", User.class))
                .thenApply(userCollection -> Json.toJson(userCollection.find().into(new ArrayList<>())))
                .thenApply(Results::ok)
                .exceptionally(result -> badRequest(result.getLocalizedMessage()));
    }

    public CompletionStage<Result> save(Http.Request request) {
        return CompletableFuture
                .supplyAsync(() -> mongoDB.getMongoDatabase())
                .thenApply(training -> training.getCollection("users", User.class))
                .thenApplyAsync(userCollection -> {
                    User user = getUserFromJson(request);

                    if (user == null) return badRequest("Bad request");

                    String message = HibernateValidator.validate(user);
                    if (!Strings.isNullOrEmpty(message)) return badRequest("Error while validating model!");

                    userCollection.insertOne(user);

                    return ok(Json.toJson(user));
                }, ec.current())
                .exceptionally(error -> badRequest(error.getLocalizedMessage()));
    }

    public CompletionStage<Result> update(Http.Request request, String id) {
        return CompletableFuture
                .supplyAsync(() -> mongoDB.getMongoDatabase())
                .thenApply(training -> training.getCollection("users", User.class))
                .thenApply(userCollection -> {
                    User user = getUserFromJson(request);

                    if (user == null) return badRequest("Bad request");

                    User findUser = userCollection.findOneAndReplace(Filters.eq("id", id), user);

                    if (findUser == null) return notFound("User not found!");

                    return ok(Json.toJson(findUser));
                });
    }

    public CompletionStage<Result> delete(Http.Request request, String id) {
        return CompletableFuture
                .supplyAsync(() -> mongoDB.getMongoDatabase())
                .thenApply(training -> training.getCollection("users", User.class))
                .thenApply(userCollection -> {
                    DeleteResult deletedUser = userCollection.deleteOne(Filters.eq("id", id));

                    if (deletedUser.getDeletedCount() == 0) return notFound("User not found!");

                    return ok(Json.toJson(id));
        });
    }

    private User getUserFromJson(Http.Request request) {
        try {
            return Json.fromJson(request.body().asJson(), User.class);
        } catch (Exception ex) {
            return null;
        }
    }
}
