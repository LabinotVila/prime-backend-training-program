package io.training.api.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import io.training.api.models.User;
import io.training.api.models.validators.HibernateValidator;
import play.cache.NamedCache;
import play.cache.redis.AsyncCacheApi;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.*;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletionStage;

public class AssignmentLectureSixController extends Controller {
    @Inject
    HttpExecutionContext ec;

    @Inject
    @NamedCache("redis")
    AsyncCacheApi cacheApi;

    ObjectMapper mapper = new ObjectMapper();
    private final String KEY = "users";

    public CompletionStage<Result> setup(Http.Request request) {
        return cacheApi.getOrElse(KEY, () -> {
            User[] userArray = Json.fromJson(request.body().asJson(), User[].class);
            ArrayList<User> userList = new ArrayList<>(Arrays.asList(userArray));

            String message = HibernateValidator.validate(userList);
            if (!Strings.isNullOrEmpty(message)) return badRequest("Error while validating model!");

            return mapper.writeValueAsString(userList);
        })
                .thenApply(this::cacheToArrayList)
                .thenApply(result -> ok(Json.toJson(result)));
    }

    public CompletionStage<Result> all(Http.Request request) {
        return getCache().thenApply(cache -> ok(Json.toJson(cache)));
    }


    public CompletionStage<Result> save(Http.Request request) {
        return getCache().thenApply(this::cacheToArrayList)
                .thenApplyAsync(list -> {
                    User user = Json.fromJson(request.body().asJson(), User.class);
                    user.setId(UUID.randomUUID().toString());

                    String messages = HibernateValidator.validate(user);
                    if (!Strings.isNullOrEmpty(messages)) return badRequest("Bad user model!");

                    list.add(user);

                    if (writeToCacheFailed(list)) return badRequest("Error while writing to cache!");

                    return ok(Json.toJson(list));
                }, ec.current())
                .thenApply(response -> response)
                .exceptionally(error -> badRequest("Something went wrong!"));
    }

    public CompletionStage<Result> update(Http.Request request, String id) {
        return getCache().thenApply(this::cacheToArrayList)
                .thenApplyAsync(list -> {
                    Optional<User> optUser = list.stream().filter(x -> x.getId().equals(id)).findFirst();
                    if (!optUser.isPresent()) return notFound("User not found!");

                    String messages = HibernateValidator.validate(optUser);
                    if (!Strings.isNullOrEmpty(messages)) return badRequest("Bad user model!");

                    User user = Json.fromJson(request.body().asJson(), User.class);
                    list.set(list.indexOf(optUser.get()), user);

                    if (writeToCacheFailed(list)) return badRequest("Error while writing");

                    return ok(Json.toJson(list));
                }, ec.current())
                .thenApply(result -> result)
                .exceptionally(error -> badRequest("Something went wrong!"));
    }

    public CompletionStage<Result> delete(Http.Request request, String id) {
        return getCache().thenApply(this::cacheToArrayList)
                .thenApplyAsync(list -> {
                    Optional<User> user = list.stream().filter(x -> x.getId().equals(id)).findFirst();
                    if (!user.isPresent()) return notFound("User is not found!");

                    String messages = HibernateValidator.validate(user);
                    if (!Strings.isNullOrEmpty(messages)) return badRequest("Bad user model!");

                    list.remove(user.get());

                    if (writeToCacheFailed(list)) return badRequest("Error while writing to cache!");

                    return ok(Json.toJson(list));
                }, ec.current())
                .thenApply(result -> result)
                .exceptionally(error -> badRequest("Something went wrong!"));
    }

    private CompletionStage<Object> getCache() {
        return cacheApi.getOptional(KEY).thenApply(cache -> cache.orElseGet(() -> notFound("No cache found")));
    }

    private boolean writeToCacheFailed(Object object) {
        try {
            cacheApi.set(KEY, mapper.writeValueAsString(object));

            return false;
        } catch (JsonProcessingException e) {
            return true;
        }
    }

    private ArrayList<User> cacheToArrayList(Object result) {
        try {
            return new ArrayList<>(Arrays.asList(mapper.readValue(result.toString(), User[].class)));
        } catch (IOException ex) {
            return null;
        }
    }
}