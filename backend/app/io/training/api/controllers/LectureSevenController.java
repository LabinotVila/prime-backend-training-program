package io.training.api.controllers;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.stream.Materializer;
import com.fasterxml.jackson.databind.JsonNode;
import io.training.api.actors.ConfiguredActorProtocol;
import io.training.api.actors.HelloActor;
import io.training.api.actors.HelloActorProtocol;
import io.training.api.actors.MyWebSocketActor;
import io.training.api.models.Hierarchy;
import io.training.api.models.User;
import io.training.api.models.backtracking.Node;
import io.training.api.models.backtracking.Nodes;
import io.training.api.models.rectangles.PointRect;
import io.training.api.models.rectangles.Rectangle;
import io.training.api.models.requests.TypeValuePair;
import play.cache.SyncCacheApi;
import play.cache.NamedCache;
import play.libs.Json;
import play.libs.streams.ActorFlow;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;
import scala.compat.java8.FutureConverters;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import play.libs.streams.ActorFlow;
import play.mvc.*;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static akka.pattern.Patterns.ask;

@Singleton
public class LectureSevenController extends Controller {
    private final ActorRef helloActor;
    private ActorRef configuredActor;
    private final ActorSystem actorSystem;
    private final Materializer materializer;

    @com.google.inject.Inject
    @NamedCache("redis")
    SyncCacheApi cache;

    @Inject
    public LectureSevenController(ActorSystem system, Materializer materializer, @Named("configured-actor") ActorRef configuredActor) {
        this.helloActor = system.actorOf(HelloActor.getProps());
        this.configuredActor = configuredActor;
        this.actorSystem = system;
        this.materializer = materializer;
    }


    // Find the top 4 richest users above 40 years old, based on their total assets!
    public CompletionStage<Result> richest(Http.Request request) {
        return CompletableFuture.supplyAsync(() -> {
            User[] userArray = Json.fromJson(request.body().asJson(), User[].class);
            List<User> userList = new ArrayList<>(Arrays.asList(userArray));

            List<User> result = userList.stream()
                    .filter(user -> user.getAge() > 40)
                    .peek(value -> value.setTotal(value.getAssets().stream().mapToInt(TypeValuePair::getValue).sum()))
                    .sorted((o1, o2) -> o2.getTotal() - o1.getTotal()).limit(4).collect(Collectors.toList());

            return Json.toJson(result);
        })
                .thenApply(Results::ok)
                .exceptionally(response -> badRequest("Something went wrong!"));
    }

    // Sort the users by their first name and last name initials (Agon Lohaj -> AL). If initials repeat, then sort on Age. Both initials and Age are sorted descending
    public CompletionStage<Result> sorted(Http.Request request) {
        return CompletableFuture.supplyAsync(() -> {
            User[] userArray = Json.fromJson(request.body().asJson(), User[].class);
            List<User> userList = new ArrayList<>(Arrays.asList(userArray));

            Comparator<User> compareUsers = Comparator.comparing(u -> u.getName().substring(0, 1) + u.getLastName().substring(0, 1));
            Comparator<User> withAge = compareUsers.thenComparing(User::getAge);

            Comparator<User> reversedOrder = withAge.reversed();

            List<User> response = userList.stream()
                    .sorted(reversedOrder)
                    .collect(Collectors.toList());

            return Json.toJson(response);
        })
                .thenApply(Results::ok)
                .exceptionally(response -> badRequest("Something went wrong!"));
    }

    //For the given rectangles and a point check if the point is contained within one of the rectangles. If so, return the
    //rectangle otherwise return the closest rectangle to the point! A rectangle is said to be the closest if the distance between its center and the point is lowest!
    public CompletionStage<Result> closest(Http.Request request) {
        return CompletableFuture.supplyAsync(() -> {
            PointRect pointRect = Json.fromJson(request.body().asJson(), PointRect.class);

            Predicate<Rectangle> hasPoint = rectangle -> rectangle.containsPoint(pointRect.getPoint());
            List<Rectangle> response = pointRect.rectangles.stream().filter(hasPoint).collect(Collectors.toList());

            Consumer<Rectangle> calcDistance = (rectangle) -> {
                double[] center = new double[]{
                        rectangle.getX() + (rectangle.getWidth()) / 2,
                        rectangle.getY() + (rectangle.getHeight()) / 2
                };
                double first = Math.pow(center[0] - pointRect.point.getX(), 2);
                double second = Math.pow(center[1] - pointRect.point.getY(), 2);

                rectangle.setDistance(Math.sqrt(first + second));
            };

            if (response.size() != 0) return Json.toJson(response.get(0));

            response = pointRect.rectangles.stream()
                    .peek(calcDistance)
                    .sorted(Comparator.comparing(Rectangle::getDistance))
                    .collect(Collectors.toList());

            System.out.println(response.get(0));

            return response.size() == 0 ? Json.toJson(null) : Json.toJson(response.get(0));
        })
                .thenApply(Results::ok)
                .exceptionally(response -> badRequest("Something went wrong!"));
    }

    //Return a new list, for which the companies are nested based on their hierarchy!
    public List<Hierarchy> callIt(List<Hierarchy> all, List<Hierarchy> nodes) {
        return nodes.stream().peek(next -> next.setChildren(callIt(
                all,
                all.stream()
                        .filter(which -> which.getParentId() != null && which.getParentId().equals(next.getId()))
                        .collect(Collectors.toList())
        ))).collect(Collectors.toList());
    }

    public CompletionStage<Result> hierarchy(Http.Request request) {
        return CompletableFuture.supplyAsync(() -> {

            List<Hierarchy> all = Arrays.asList(Json.fromJson(request.body().asJson(), Hierarchy[].class));

            List<Hierarchy> result = callIt(
                    all,
                    all.stream().filter(a -> a.getParentId() == null).collect(Collectors.toList())
            );

            return Json.toJson(result);
        })
                .thenApply(Results::ok)
                .exceptionally(response -> badRequest(response.toString()));
    }

    public CompletionStage<Result> restricted(Http.Request request) {
        return CompletableFuture.supplyAsync(() -> {

            Optional<String> headerExists = request.getHeaders().get("Authorization");

            if (headerExists.isPresent() && headerExists.get().equals("Admin")) {
                return ok(Json.toJson("Found"));
            } else {
                return status(403, Json.toJson("Not found!"));
            }
        });
    }

    public Result cached(Http.Request request) {
        Optional<Object> optKey = cache.getOptional("key");

        if (optKey.isPresent()) {
            return status(200, Json.toJson(optKey.get()));
        } else {
            String response = request.body().asJson().asText();

            cache.set("key", response, 1);

            return status(200, Json.toJson(response));
        }
    }

    public CompletionStage<Result> backtracking(Http.Request request) {
        return CompletableFuture.supplyAsync(() -> {

            Nodes problem = Json.fromJson(request.body().asJson(), Nodes.class);

            List<List<Integer>> result = Backtrack.solve(problem);

            return ok(Json.toJson(result));
        });
    }

    public CompletionStage<Result> sayHello(String name) {
        return FutureConverters.toJava(ask(helloActor, new HelloActorProtocol.SayHello(name), 1000))
                .thenApply(response -> ok((JsonNode) response));
    }

    public CompletionStage<Result> getConfig() {
        return FutureConverters.toJava(ask(configuredActor, new ConfiguredActorProtocol.GetConfig(), 1000))
                .thenApply(response -> ok((String) response));
    }

    public WebSocket socket() {
        return WebSocket.Text.accept(
                request -> ActorFlow.actorRef(MyWebSocketActor::props, actorSystem, materializer));
    }
}