package io.training.api.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.training.api.models.BinaryTree;
import io.training.api.models.User;
import io.training.api.models.requests.NameValuePair;
import play.libs.Json;
import play.mvc.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AssignmentLectureThreeController extends Controller {
    private static List<User> users = new ArrayList<>();

    public Result averages(Http.Request request) {

        List<NameValuePair> response = users
                .stream()
                .collect(Collectors.groupingBy(User::getType, Collectors.averagingInt(User::getAge)))
                .entrySet()
                .stream()
                .map(x -> new NameValuePair(x.getKey(), (int) Math.round(x.getValue()))).collect(Collectors.toList());

        return ok(Json.toJson(response));
    }

    public Result types(Http.Request request) {
        List<NameValuePair> response = users.stream()
                .collect(Collectors.groupingBy(User::getGender, Collectors.counting()))
                .entrySet()
                .stream()
                .map(x -> new NameValuePair(x.getKey(), Math.toIntExact(x.getValue())))
                .collect(Collectors.toList());

        return ok(Json.toJson(response));
    }

    public Result save(Http.Request request) {
        User user = Json.fromJson(Json.toJson(request.body().asJson()), User.class);
        user.setId(UUID.randomUUID().toString());

        users.add(user);

        return ok(Json.toJson(user));
    }

    public Result update(Http.Request request, String id) {
        Optional<User> userWithId = users.stream().filter(x -> x.getId().equals(id)).findFirst();

        if (!userWithId.isPresent()) {
            return badRequest("Specified user does not exist!");
        }

        int userIndex = users.indexOf(userWithId.get());

        User updatedUser = Json.fromJson(Json.toJson(request.body().asJson()), User.class);

        users.set(userIndex, updatedUser);

        return ok(Json.toJson(updatedUser));
    }

    public Result delete(Http.Request request, String id) {
        Optional<User> userWithId = users.stream().filter(x -> x.getId().equals(id)).findFirst();

        if (!userWithId.isPresent()) {
            return badRequest("Specified user does not exist!");
        }

        int userIndex = users.indexOf(userWithId.get());

        User deletedUser = users.remove(userIndex);

        return ok(Json.toJson(deletedUser));
    }

    public Result all(Http.Request request) {
        return ok(Json.toJson(users));
    }

    private BinaryTree binaryTreeSearch(BinaryTree root, int value) {
        if (root == null || root.getValue() == value) return root;

        if (root.getValue() > value) return binaryTreeSearch(root.getLeft(), value);

        return binaryTreeSearch(root.getRight(), value);
    }

    public Result binaryTree(Http.Request request) {
        int value = request.body().asJson().get("value").asInt();
        JsonNode tree = request.body().asJson().get("tree");

        BinaryTree result = binaryTreeSearch(Json.fromJson(tree, BinaryTree.class), value);

        JsonNode resultToJson = result == null ? Json.newObject() : Json.toJson(result);

        return ok(resultToJson);
    }

}