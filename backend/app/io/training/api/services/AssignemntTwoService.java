package io.training.api.services;

import com.google.common.primitives.Ints;
import com.google.inject.Inject;
import io.training.api.models.requests.BinarySearchRequest;
import io.training.api.models.requests.NameValuePair;
import io.training.api.models.responses.ChartData;
import play.Logger;
import play.libs.concurrent.HttpExecutionContext;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Agon on 09/08/2020
 */
@Singleton
public class AssignemntTwoService {
    @Inject
    HttpExecutionContext ec;


    /**
     * Function as Line: y = x * 2
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<Integer>> function1(List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> input.stream().map(x -> x * 2).collect(Collectors.toList()), ec.current());
    }


    /**
     * Function as Scatter y = square root of the absolute value of ((x ^ 2) + (x * 4))
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function2(List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            Function<Integer, Double> expression = (x) -> Math.sqrt(Math.abs(Math.pow(x, 2) + x * 4));

            return input.stream().map(expression).collect(Collectors.toList());
        }, ec.current());
    }

    /**
     * Function y = If 3^2 - x^2 > 0 than square root of (3^2 - x^2). If 3^2 - x^2 < 0 then - square root of absolute value of (3^2 - x^2)
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function3(List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            Function<Integer, Double> first = (x) -> Math.pow(3, 2) - Math.pow(x, 2);
            Function<Integer, Double> second = (x) -> Math.sqrt(first.apply(x));
            Function<Integer, Double> third = (x) -> -1 * Math.sqrt(Math.abs(first.apply(x)));

            Function<Integer, Double> test = (x) -> first.apply(x) > 0 ? second.apply(x) : third.apply(x);

            return input.stream().map(test).collect(Collectors.toList());
        }, ec.current());
    }

    /**
     * Function as Line: y = sin(x)
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function4(List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> input.stream().map(Math::sin).collect(Collectors.toList()), ec.current());
    }

    /**
     * Function as Line: y = cos(x)
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function5(List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> input.stream().map(Math::cos).collect(Collectors.toList()), ec.current());
    }

    /**
     * 2 Line Functions displayed together, one for Sin and one for Cos, check only the series option to include two of them
     * Returns two lists of double the first list for sin, and the second one for cos
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<List<Double>>> function6(List<Integer> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> new ArrayList<List<Double>>() {
            {
                add(input.stream().map(Math::sin).collect(Collectors.toList()));
                add(input.stream().map(Math::cos).collect(Collectors.toList()));
            }
        }, ec.current());
    }

    /**
     * I want to see the top 4 performing words, given the randomCategoryData, the top 4 with the highest random generated value
     * Make sure the values are summed on repeated words (no duplicates)
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<NameValuePair>> function7(List<NameValuePair> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            try {
                return input
                        .stream()
                        .collect(Collectors.groupingBy(NameValuePair::getName, Collectors.summingInt(NameValuePair::getValue)))
                        .entrySet()
                        .stream().sorted((o1, o2) -> o2.getValue() - o1.getValue())
                        .limit(4)
                        .map(a -> new NameValuePair(a.getKey(), a.getValue()))
                        .collect(Collectors.toList());
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new CompletionException(ex);
            }
        }, ec.current());
    }

    /**
     * Calculate the average within the groups now, and show that here. Check the random Category data on how it generates those
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<ChartData>> function8(List<NameValuePair> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> input
                .stream()
                .collect(Collectors.groupingBy(NameValuePair::getName, Collectors.averagingInt(NameValuePair::getValue)))
                .entrySet()
                .stream()
                .map(a -> new ChartData(a.getKey(), a.getValue())).collect(Collectors.toList()), ec.current());
    }

    /**
     * Calculate the values such that they are cumulative, each subsequent is summed with the total so far!
     *
     * @param input
     * @return
     */
    public CompletableFuture<List<NameValuePair>> function9(List<NameValuePair> input) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> input.stream().reduce(new ArrayList<>(), (acc, element) -> {
            acc.add(
                    acc.size() == 0 ? element : new NameValuePair(element.getName(), acc.get(acc.size() - 1).getValue() + element.getValue())
            );

            return acc;
        }, (a, b) -> a));
    }

    /**
     * Binary search algorithm
     *
     * @param arr input array
     * @param l minimum index
     * @param r maximum index
     * @param x value to be serached
     * @return found result
     */
    int binarySearch(int arr[], int l, int r, int x)
    {
        if (r >= l) {
            int mid = l + (r - l) / 2;

            if (arr[mid] == x) return mid;

            if (arr[mid] > x) return binarySearch(arr, l, mid - 1, x);

            return binarySearch(arr, mid + 1, r, x);
        }

        return -1;
    }

    /**
     * Using Binary Search I will search for the given value at the given sorted array
     *
     * @param request
     * @return index - the index of the search
     */
    public CompletableFuture<Integer> binarySearch(BinarySearchRequest request) {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> binarySearch(Ints.toArray(request.getValues()), 0, request.getValues().size() - 1, request.getSearch()), ec.current());
    }
}
