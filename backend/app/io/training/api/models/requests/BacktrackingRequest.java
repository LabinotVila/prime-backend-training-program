package io.training.api.models.requests;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by agonlohaj on 20 Aug, 2020
 */
@Data
public class BacktrackingRequest {
	private List<Node> nodes = new ArrayList<>();
	private Integer from;
	private Integer to;
}
