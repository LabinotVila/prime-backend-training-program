package io.training.api.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.training.api.models.requests.TypeValuePair;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.*;
import java.util.List;

/**
 * Created by agonlohaj on 28 Aug, 2020
 */
@Data
@EqualsAndHashCode(of = "id")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {
    String id;
    @NotNull(message = "can not be null!")
    String type;
    @Pattern(regexp = "[MF]")
    String gender;
    @Min(value = 0, message = "cannot be lower than 0")
    @Max(value = 90, message = "cannot be higher than 100")
    int age;
    @NotNull(message = "can not be null!")
    String name;
    @NotNull(message = "can not be null!")
    String username;
    @NotNull(message = "can not be null!")
    String lastName;
    @NotNull(message = "can not be null!")
    String avatar;
    List<TypeValuePair> assets;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    int total;
}
