package io.training.api.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

public @Data
class Hierarchy {
    String id;
    String name;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    String parentId;
    List<Hierarchy> children;
}
