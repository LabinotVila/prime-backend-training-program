package io.training.api.models.rectangles;

import lombok.Data;

public @Data
class Point {
    int x;
    int y;
}
