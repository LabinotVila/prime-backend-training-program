package io.training.api.models.rectangles;

import lombok.Data;

public @Data
class Rectangle {
    String id;
    int x;
    int y;
    double width;
    double height;
    double distance;

    public boolean containsPoint(Point point)
    {
        return point.x >= x && (point.x <= x + width) && point.y >= y && (point.y <= y + height);
    }
}
