package io.training.api.models.rectangles;

import lombok.Data;

import java.util.List;

public @Data
class PointRect {
    public Point point;
    public List<Rectangle> rectangles;
}
