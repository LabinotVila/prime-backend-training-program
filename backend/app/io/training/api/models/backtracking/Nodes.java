package io.training.api.models.backtracking;

import lombok.Data;

import java.util.List;

public @Data
class Nodes {
    List<Node> nodes;
    int from;
    int to;
}
