package io.training.api.models.backtracking;

import lombok.Data;

import java.util.List;

public @Data
class Node {
    int id;
    List<Integer> links;
}
